<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Api Message
    |--------------------------------------------------------------------------
    |
    |success and failure messages
    |
    |
    */
    'errors' => [
        'commentLimit'=>'more than 2 comments for each product not allow'
    ],

    'addComment' => 'comment added',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
