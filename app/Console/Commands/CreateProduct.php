<?php

namespace App\Console\Commands;

use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ValidatedInput;

class CreateProduct extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:insert {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * The name and signature of the console command.
     *
     * @var ProductRepositoryInterface
     */
    protected $repository = 'product:insert {name: Product name}';

    public function __construct(ProductRepositoryInterface $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $validator = Validator::make($this->arguments(), [
            'name' => ['required','string', 'unique:products'],
        ]);
        if ($validator->fails()) {
            $this->error('Whoops! The given attributes are invalid.');
            collect($validator->errors()->all())
                ->each(fn ($error) => $this->line($error));
            exit;
        }
        $product = $this->repository->create(['name' => $this->argument('name')]);
        $this->info('product created!');
        $this->line('--------------');
        $this->line($product);
        $this->newLine(1);
    }


}
