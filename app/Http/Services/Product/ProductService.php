<?php

namespace App\Http\Services\Product;

use App\Helper\Response\ApiResponseFacade;
use App\Http\Services\ServiceInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductService implements ServiceInterface
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function productsWithComments() {
        return $this->repository->productsWithComments(['id','name']);
    }

}
