<?php

namespace App\Http\Services\Comment;

use App\Http\Services\ServiceInterface;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\ProductCommentFile\ProductCommentFileRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CommentService implements ServiceInterface
{
    private $repository;
    private $productRepository;
    private $fileRepository;

    public function __construct(CommentRepositoryInterface            $repository,
                                ProductRepositoryInterface            $productRepository,
                                ProductCommentFileRepositoryInterface $fileRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param $request
     * @return void
     */
    public function create($request)
    {

        $productName = $request['product_name'];
        $userId = Auth::user()->id;
        $product = $this->productRepository->findByName($productName);
        if (!$product) {
            $product = $this->productRepository->create(['name' => $productName]);
        }
        $this->productRepository->userCommentsCount($product, $userId);
        if ($product['comments_count'] >= 2) {
            throw new BadRequestException(trans('messages.errors.commentLimit'));
        }

        $this->repository->create(
            [
                'message' => $request['message'],
                'user_id' => $userId,
                'product_id' => $product['id'],
            ]);
        $dbCount = $this->productRepository->commentsCount($product);
        $this->updateCommentFile($product['name'], $dbCount);
    }

    /**
     * @param $productName
     * @param $dbCount
     * @return void
     */
    public function updateCommentFile($productName, $dbCount)
    {

        $this->fileRepository->create();
        if ($dbCount <= 1) {
            $this->fileRepository->addNewLine($productName, $dbCount);
        } else {
           $this->fileRepository->update($productName, $dbCount);
        }
    }

}
