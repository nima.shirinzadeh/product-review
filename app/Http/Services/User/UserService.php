<?php

namespace App\Http\Services\User;

use App\Http\Services\ServiceInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class UserService implements ServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $request
     * @return void
     */
    public function signup($request)
    {
        $this->repository->create(['username' => $request['username'], 'password' => bcrypt($request['password'])]);
    }

    /**
     * @return JsonResponse
     */
    public function signin()
    {
        $credentials = request(['username', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            throw new BadRequestException(trans('auth.failed'));
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param $token
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * config('jwt.ttl')
        ]);
    }

    /**
     * @return Authenticatable|null
     */
    public function user() {
        return auth()->user();
    }
}
