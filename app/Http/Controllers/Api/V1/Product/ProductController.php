<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Helper\Response\ApiResponseFacade;
use App\Http\Controllers\Controller;
use App\Http\Services\Product\ProductAbstractService;
use App\Http\Services\Product\ProductService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $service;
    private $response;

    public function __construct(ProductService $service, ApiResponseFacade $response)
    {
        $this->middleware('auth:api');
        $this->service = $service;
        $this->response = $response;

    }

    /**
     * @return JsonResponse
     */
    public function list()
    {
        try {
            return $this->response->code200(null, $this->service->productsWithComments());
        } catch (\Exception $exception) {
            return $this->response->code500();
        }
    }
}
