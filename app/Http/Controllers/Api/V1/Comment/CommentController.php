<?php

namespace App\Http\Controllers\Api\V1\Comment;

use App\Helper\Response\ApiResponseFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\AddCommentRequest;
use App\Http\Services\Comment\CommentService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CommentController extends Controller
{
    private $service;
    private $response;

    public function __construct(CommentService $service, ApiResponseFacade $response)
    {
        $this->middleware('auth:api');
        $this->service = $service;
        $this->response = $response;

    }

    /**
     * @param AddCommentRequest $request
     * @return JsonResponse
     */
    public function add(AddCommentRequest $request)
    {
        try {
            return $this->response->code200(trans('messages.addComment'),$this->service->create($request));
        } catch (BadRequestException $exception) {
            return $this->response->code400($exception->getMessage());
        }catch (\Exception $exception) {
            return $this->response->code500($exception->getMessage());
        }
    }
}
