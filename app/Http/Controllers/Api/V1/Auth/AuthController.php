<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Helper\Response\ApiResponseFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserSigninRequest;
use App\Http\Requests\User\UserSignupRequest;
use App\Http\Services\User\UserService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class AuthController extends Controller
{
    private $service;
    private $response;

    public function __construct(UserService $service, ApiResponseFacade $response)
    {
        $this->middleware('auth:api', ['except' => ['signup', 'signin']]);
        $this->service = $service;
        $this->response = $response;

    }

    public function signup(UserSignupRequest $request)
    {
        try {
            $this->service->signup($request);
            return $this->response->code200(trans('auth.signup'));
        } catch (\Exception $exception) {
            return $this->response->code500();
        }
    }

    public function signin(UserSigninRequest $request)
    {
        try {
            return $this->response->code200(trans('auth.signin'),$this->service->signin());
        } catch (BadRequestException $exception) {
            return $this->response->code400($exception->getMessage());
        } catch (\Exception $exception) {
            return $this->response->code500();
        }
    }

    public function user()
    {
        try {
            return $this->response->code200($this->service->user());
        } catch (\Exception $exception) {
            return $this->response->code500();
        }
    }
}
