<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserSignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|alpha_num:ascii|unique:users|min:4|max:40',
            'password' => 'required|alpha_num|confirmed|min:8|max:25',
            'password_confirmation' => 'alpha_num|string|min:8|max:25',
        ];
    }
}
