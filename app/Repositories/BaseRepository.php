<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Ramsey\Collection\Collection;

interface BaseRepository
{
    public function find($id): ?Model;

    public function all(array $attributes);

    public function create(array $attributes): Model;

    public function update(Model $model, array $attributes);

    public function delete(Model $model);

}
