<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepository;

interface ProductRepositoryInterface extends BaseRepository
{
    public function productsWithComments();
    public function findByName($name);
    public function userCommentsCount(Product $product,$userId);
    public function commentsCount($product);

}
