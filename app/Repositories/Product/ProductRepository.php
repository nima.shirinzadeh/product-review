<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;

class ProductRepository implements ProductRepositoryInterface
{
    use RepositoryTrait;

    public function find($id): ?Model
    {
        // TODO: Implement find() method.
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function all(array $attributes)
    {
        return $this->model->paginate(10);
    }

    /**
     * @param $fields
     * @return mixed
     */
    public function productsWithComments($fields = '*')
    {
        return $this->model->select($fields)->with('comments:id,product_id,user_id,message')->paginate(10);
    }

    /**
     * @param Product $product
     * @param $userId
     * @return Product
     */
    public function userCommentsCount(Product $product, $userId)
    {
        return $product->loadCount(['comments' => function ($query) use ($userId) {
            $query->where('user_id', $userId);
        }]);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function findByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    public function update(Model $model, array $attributes)
    {
        // TODO: Implement update() method.
    }

    public function delete(Model $model)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $product
     * @return mixed
     */
    public function commentsCount($product)
    {
        return $product->comments()->count();
    }
}
