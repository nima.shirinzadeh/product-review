<?php

namespace App\Repositories\Comment;

use App\Repositories\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;

class CommentRepository implements CommentRepositoryInterface
{
    use RepositoryTrait;

    public function find($id): ?Model
    {
        // TODO: Implement find() method.
    }

    public function all(array $attributes)
    {
        return $this->model->paginate(10);
    }

    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    public function update(Model $model, array $attributes)
    {
        // TODO: Implement update() method.
    }

    public function delete(Model $model)
    {
        // TODO: Implement delete() method.
    }

}
