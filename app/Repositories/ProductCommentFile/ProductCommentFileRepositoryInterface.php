<?php

namespace App\Repositories\ProductCommentFile;

interface ProductCommentFileRepositoryInterface
{
    public function create();

    public function addNewLine(string $name, string $count);

    public function update(string $searchName, string $replaceCount);

}
