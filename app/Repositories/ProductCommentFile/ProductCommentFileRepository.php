<?php

namespace App\Repositories\ProductCommentFile;

class ProductCommentFileRepository implements ProductCommentFileRepositoryInterface
{
    private $path = '/opt/myprogram';
    private $fileName = 'product_comments';
    private $fullPath;
    public function __construct()
    {
        $this->fullPath = "{$this->path}/{$this->fileName}";
    }

    /**
     * @return void
     */
    public function create()
    {
        exec("mkdir -p {$this->path} && touch {$this->fullPath}");
    }


    /**
     * @param $name
     * @param $count
     * @return void
     */
    public function addNewLine($name, $count)
    {
        $name = escapeshellcmd($name);
        exec("echo {$name}:{$count} >> {$this->fullPath}");
    }


    /**
     * @param $searchName
     * @param $replaceCount
     * @return void
     */
    public function update($searchName, $replaceCount)
    {
        $searchName = escapeshellcmd($searchName);
        $replaceCount = escapeshellcmd($replaceCount);
        $cmd = "sed -i 's/\b{$searchName}:[[:digit:]]\+\b/{$searchName}:{$replaceCount}/' {$this->fullPath}";
        exec($cmd);
    }
}
