<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;

trait RepositoryTrait
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
