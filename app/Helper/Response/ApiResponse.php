<?php

namespace App\Helper\Response;


use Illuminate\Support\Facades\Response;

class ApiResponse
{
    private $messages;
    private $errors;
    private $body;
    private $ok;
    private $code;
    private $header=[];


    /**
     * @param mixed $messages
     */
    public function setMessages($messages): void
    {
        $this->messages = $messages;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    /**
     * @param bool $ok
     * @return void
     */
    public function setOk(bool $ok): void
    {
        $this->ok = $ok;
    }

    /**
     * @param int $code
     * @return void
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    public function response()
    {
        return
            [
            'messages' => $this->messages,
            'errors' => $this->errors,
            'body' => $this->body,
            'ok' => $this->ok,
            'code' => $this->code
        ];
    }
}
