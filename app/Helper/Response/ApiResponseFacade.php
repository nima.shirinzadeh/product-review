<?php

namespace App\Helper\Response;

class ApiResponseFacade
{
    protected $success;
    protected $error;

    public function __construct(ApiResponseBuilder $success, ApiResponseBuilder $error)
    {
        $this->success = $success;
        $this->error = $error;
        $this->success->createApiResponse();
        $this->success->setOk();
        $this->error->createApiResponse();
        $this->error->setOk();

    }

    private function success()
    {
        $response = $this->success->getApiResponse()->response();
        return response()->json($response,$response['code'],[]);

    }
    private function error()
    {
        $response =  $this->error->getApiResponse()->response();
        return response()->json($response,$response['code'],[]);

    }

    public function code200($message = null, $body = null)
    {
        $this->success->setCode(200);
        $this->success->setMessages($message);
        $this->success->setBody($body);
        return $this->success();
    }

    public function code400($message=null)
    {
        $this->error->setCode(400);
        $this->error->setErrors($message);
        return $this->error();
    }

    public function code500($message='Server Error')
    {
        $this->error->setCode(500);
        $this->error->setErrors($message);
        return $this->error();
    }

    public function code401($message='unauthenticate')
    {
        $this->error->setCode(401);
        $this->error->setErrors($message);
        return $this->error();
    }
}
