<?php

namespace App\Helper\Response;

class ErrorApiResponseBuilder extends ApiResponseBuilder
{

    public function setMessages($messages): void
    {
    }

    public function setErrors($errors): void
    {
        $this->apiResponse->setErrors($errors);

    }

    public function setBody($body): void
    {
    }

    public function setOk($ok=false): void
    {
        $this->apiResponse->setOk($ok);
    }

    public function setCode($code): void
    {
        $this->apiResponse->setCode($code);
    }
}
