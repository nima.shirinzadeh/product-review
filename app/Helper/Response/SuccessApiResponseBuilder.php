<?php

namespace App\Helper\Response;

class SuccessApiResponseBuilder extends ApiResponseBuilder
{

    public function setMessages($messages): void
    {
        $this->apiResponse->setMessages($messages);
    }

    public function setErrors($errors): void
    {
    }

    public function setBody($body): void
    {
        $this->apiResponse->setBody($body);
    }

    public function setOk($ok=true): void
    {
        $this->apiResponse->setOk($ok);
    }

    public function setCode($code): void
    {
        $this->apiResponse->setCode($code);
    }
}
