<?php

namespace App\Helper\Response;

abstract class ApiResponseBuilder
{
    /**
     * @var ApiResponse
     */
    protected $apiResponse;

    public function createApiResponse()
    {
        $this->apiResponse = new ApiResponse();
    }

    /**
     * @param mixed $messages
     */
    abstract public function setMessages($messages): void;

    /**
     * @param mixed $errors
     */
    abstract public function setErrors($errors): void;

    /**
     * @param mixed $body
     */
    abstract public function setBody($body): void;

    /**
     * @param mixed $ok
     */
    abstract public function setOk($ok): void;

    /**
     * @param mixed $code
     */
    abstract public function setCode($code): void;

    public function getApiResponse() {
        return $this->apiResponse;
    }
}
