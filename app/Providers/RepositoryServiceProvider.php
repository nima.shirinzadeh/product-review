<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Product;
use App\Models\User;
use App\Repositories\Comment\CommentRepository;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\ProductCommentFile\ProductCommentFileRepository;
use App\Repositories\ProductCommentFile\ProductCommentFileRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //user
        $this->app->bind(UserRepositoryInterface::class, function (Application $app) {
            return new UserRepository($app->make(User::class));
        });

        //product
        $this->app->bind(ProductRepositoryInterface::class, function (Application $app) {
            return new ProductRepository($app->make(Product::class));
        });

        //product
        $this->app->bind(CommentRepositoryInterface::class, function (Application $app) {
            return new CommentRepository($app->make(Comment::class));
        });

        //product
        $this->app->bind(ProductCommentFileRepositoryInterface::class,
        ProductCommentFileRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
