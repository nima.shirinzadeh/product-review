<?php

namespace App\Providers;

use App\Helper\Response\ApiResponseFacade;
use App\Helper\Response\ErrorApiResponseBuilder;
use App\Helper\Response\SuccessApiResponseBuilder;
use App\Http\Services\Product\ProductAbstractService;
use App\Http\Services\Product\ProductService;
use App\Http\Services\User\UserService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserService::class);
        $this->app->bind(ProductService::class);
        $this->app->singleton(ApiResponseFacade::class, function (Application $app) {
            return new ApiResponseFacade(
                $app->make(SuccessApiResponseBuilder::class),
                $app->make(ErrorApiResponseBuilder::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

    }
}
