<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductListTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_product_list()
    {
        $this->withoutMiddleware();
        $response = $this->get('/api/v1/products/list');
        $response->assertStatus(200);
        $res_array = (array)json_decode($response->content(),true);
        $this->assertArrayHasKey('body',$res_array);
        $this->assertArrayHasKey('current_page',$res_array['body']);
        $this->assertArrayHasKey('data',$res_array['body']);
    }
}
