<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SignupTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function test_bad_Request_error()
    {
        $response = $this->post('/api/v1/auth/signup',[],['Accept'=>'application/json']);
        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function test_sunccess_signup()
    {
        $response = $this->post('/api/v1/auth/signup',
            ['username'=>'newusername','password'=>'12345678','password_confirmation'=>'12345678'],
            ['Accept'=>'application/json']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     *
     */
    public function test_unique_username()
    {
        $response = $this->post('/api/v1/auth/signup',
            ['username'=>'parspack','password'=>'12345678','password_confirmation'=>'12345678'],
            ['Accept'=>'application/json']);

        $response->assertStatus(422)->assertJson([
            'errors' => true,
        ]);

        $res_array = (array)json_decode($response->content(),true);

        $this->assertArrayHasKey('errors', $res_array);

        $this->assertArrayHasKey('username',$res_array['errors']);
    }
}
