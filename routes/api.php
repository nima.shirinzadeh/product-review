<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1',
    'namespace' => '\App\Http\Controllers\Api\V1'

], function ($router) {

    Route::group([
        'prefix' => 'auth',
        'namespace' => 'Auth',

    ], function ($router) {

        Route::post('signup', 'AuthController@signup');
        Route::post('signin', 'AuthController@signin');

    });

    Route::group([
        'prefix' => 'products',
        'namespace' => 'Product',
    ], function ($router) {
        Route::get('list', 'ProductController@list');
    });

    Route::group([
        'prefix' => 'comments',
        'namespace' => 'Comment',
    ], function ($router) {
        Route::post('add', 'CommentController@add');
    });

});
